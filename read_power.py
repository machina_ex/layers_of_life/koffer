#!/usr/bin/env python
from ina219 import INA219
from ina219 import DeviceRangeError

import paho.mqtt.client as paho
import time
import socket 
import json

SHUNT_OHMS = 0.1
MAX_EXPECTED_AMPS = 0.2

if __name__ == "__main__":
    ina = INA219(SHUNT_OHMS, MAX_EXPECTED_AMPS)
    ina.configure(ina.RANGE_16V)
    

    client = paho.Client()
    client.connect("localhost", 1883)
    client.loop_start()

    while True:
        state = {"U":ina.voltage(), "I":ina.current(), "P":ina.power()}
        (rc, mid) = client.publish("dev/"+socket.gethostname()+"/battery",json.dumps(state), qos=1)
        time.sleep(5)
