#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#define CLK 2
#define DT 3
#define SW 4
#define ENCODERMIN 1
#define ENCODERMAX 365

volatile unsigned int encoderPos = ENCODERMIN; // a counter for the dial
unsigned int lastReportedPos = ENCODERMIN; // change management
static boolean rotating = false; // debounce management

uint8_t button = LOW;
uint8_t old_button = LOW;

// interrupt service routine variables
boolean A_set = false;
boolean B_set = false;

int month_yearday[13] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 };
uint8_t Month = 0;
uint8_t Day = 0;
char DateString[10];

boolean LOCKED = false; //

LiquidCrystal_I2C lcd(0x27, 40, 2);

void setup()
{
  pinMode(CLK, INPUT);
  pinMode(DT, INPUT);
  pinMode(SW, INPUT_PULLUP );

  digitalWrite(CLK, HIGH); // turn on pullup resistors
  digitalWrite(DT, HIGH); // turn on pullup resistors

  attachInterrupt(digitalPinToInterrupt(CLK), doEncoderA, CHANGE);
  attachInterrupt(digitalPinToInterrupt(DT), doEncoderB, CHANGE);

  lcd.init();
  lcd.clear();
  lcd.backlight();
  dayToDate();
  updateLCD();

  Serial.begin(9600);
}

void loop()
{
  // Recieve Data
  if (Serial.available() > 0) {
    String data = Serial.readStringUntil('\n');
    // TODO Read returned dayNumber
    Serial.print("You sent me: ");
    Serial.println(data);
    if (data == "UNLOCK") {
      LOCKED = false;
      updateLCD();
    }
    else if (data == "LOCK") {
      LOCKED = true;
      clearLCDLine(1);
      updateLCD();
    }
  }

  // Check Rotary Encoder
  if ( !LOCKED ) {

    rotating = true; // reset the debouncer

    if (lastReportedPos != encoderPos)
    {
      lastReportedPos = encoderPos;
      dayToDate();
      updateLCD();
    }

    button = !digitalRead( SW );

    if ( button != old_button )
    {
      if (button) { // send only on button down
        sendDate();
      }
      delay( 10 );
      old_button = button;
    }
  }

}

void updateLCD() {
  if ( LOCKED ) {
    lcd.setCursor(15, 0);
    lcd.print(DateString);
    lcd.setCursor(13, 1);
    lcd.print("WIRD ABGESPIELT");
    lcd.noBlink();
  } else {
    sprintf_P(DateString, PSTR("1990-%02d-%02d"), Month, Day);
    lcd.setCursor(15, 0);
    lcd.print(DateString);
    lcd.setCursor(2, 1);
    lcd.print("DREHEN UND KLICKEN ZUR DATUMSAUSWAHL");
    lcd.setCursor(26, 0);
    lcd.blink();
  }
}

void clearLCDLine(int line)
{
  lcd.setCursor(0, line);
  for (int n = 0; n < 40; n++)
  {
    lcd.print(" ");
  }
}

void dayToDate() {
  if (encoderPos >= 1 && encoderPos <= 365)  {
    for (int month = 0; month < 12; month++) {
      if (encoderPos <= month_yearday[month + 1]) {
        Month = month + 1;
        Day = encoderPos - month_yearday[month];
        break;
      }
    }
  } else {
    //Serial << "day of Year must be between 1 and " << daysInYear << endl;
    Month = 0;
    Day = 0;
  }
}

void sendDate()
{
  Serial.println(encoderPos);
}


// ------------- Interrupt Service Routines ------------------------------

// ISR Interrupt on A changing state (Increment position counter)
void doEncoderA()
{
  if ( LOCKED ) return;
  if ( rotating ) delay (1); // wait a little until the bouncing is done
  if ( digitalRead(CLK) != A_set ) // debounce once more
  {
    A_set = !A_set;
    // adjust counter +1 if A leads B
    if ( A_set && !B_set )
      if (encoderPos == ENCODERMAX)
        encoderPos = ENCODERMIN;
      else
        encoderPos += 1;
    rotating = false; // no more debouncing until loop() hits again
  }
}

// ISR Interrupt on B changing state, same as A above
void doEncoderB()
{
  if ( LOCKED ) return;
  if ( rotating ) delay (1);
  if ( digitalRead(DT) != B_set )
  {
    B_set = !B_set;
    //adjust counter -1 if B leads A
    if ( B_set && !A_set )
      if (encoderPos == ENCODERMIN)
        encoderPos = ENCODERMAX;
      else
        encoderPos -= 1;
    rotating = false;
  }
}
