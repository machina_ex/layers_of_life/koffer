#define EMPTY 0
#define FULL 1
#define RIGHTEMPTY 2
#define RIGHTFULL 3
#define LEFTEMPTY 4
#define LEFTFULL 5

// make some custom characters:
byte full[8] = {
  0b11111,
  0b00000,
  0b11111,
  0b11111,
  0b11111,
  0b11111,
  0b00000,
  0b11111
};

byte empty[8] = {
  0b11111,
  0b00000,
  0b00000,
  0b00000,
  0b00000,
  0b00000,
  0b00000,
  0b11111
};

byte batteryLEmpty[8] = {
  0b01111,
  0b10000,
  0b10000,
  0b10000,
  0b10000,
  0b10000,
  0b10000,
  0b01111
};

byte batteryLFull[8] = {
  0b01111,
  0b10000,
  0b10111,
  0b10111,
  0b10111,
  0b10111,
  0b10000,
  0b01111
};

byte batteryREmpty[8] = {
  0b11100,
  0b00010,
  0b00010,
  0b00011,
  0b00011,
  0b00010,
  0b00010,
  0b11100
};

byte batteryRFull[8] = {
  0b11100,
  0b00010,
  0b11010,
  0b11011,
  0b11011,
  0b11010,
  0b00010,
  0b11100
};
