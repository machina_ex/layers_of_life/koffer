void dayToDate() {
  if (encoderPos >= 1 && encoderPos <= 365)  {
    for (int month = 0; month < 12; month++) {
      if (encoderPos <= month_yearday[month + 1]) {
        Month = month + 1;
        Day = encoderPos - month_yearday[month];
        break;
      }
    }
  } else {
    //Serial << "day of Year must be between 1 and " << daysInYear << endl;
    Month = 0;
    Day = 0;
  }
}
