boolean buttonPressed() {
  boolean pressed = false;
  button = !digitalRead( SW );
  if ( button != old_button )  {
    if (button) { // send only on button down
      pressed = true;
    }
    delay( 10 );
    old_button = button;
  }
  return pressed;
}

// ------------- Interrupt Service Routines ------------------------------

// ISR Interrupt on A changing state (Increment position counter)
void doEncoderA()
{
  if ( RLOCKED ) return;
  if ( rotating ) delay (1); // wait a little until the bouncing is done
  if ( digitalRead(CLK) != A_set ) // debounce once more
  {
    A_set = !A_set;
    // adjust counter +1 if A leads B
    if ( A_set && !B_set )
      if (encoderPos == ENCODERMAX)
        encoderPos = ENCODERMIN;
      else
        encoderPos += 1;
    rotating = false; // no more debouncing until loop() hits again
  }
}

// ISR Interrupt on B changing state, same as A above
void doEncoderB()
{
  if ( RLOCKED ) return;
  if ( rotating ) delay (1);
  if ( digitalRead(DT) != B_set )
  {
    B_set = !B_set;
    //adjust counter -1 if B leads A
    if ( B_set && !A_set )
      if (encoderPos == ENCODERMIN)
        encoderPos = ENCODERMAX;
      else
        encoderPos -= 1;
    rotating = false;
  }
}
