const uint8_t BUFFER_SIZE = 82;
char buf[BUFFER_SIZE];

void readSerial() {
  if (Serial.available() > 0) {
    uint8_t rlen = Serial.readBytes(buf, BUFFER_SIZE);

    Serial.print("You sent me: ");
    for (uint8_t i = 0; i < rlen; i++) {
      Serial.print(buf[i]);
    }
    Serial.println("");

    MODE = buf[0];
    Battery = buf[1];
    buf[42] = '\0';
    Line_0 = buf + 2;
    if (MODE < TEXT) // only in TEXT Mode we use all 40 chars in buttom row (no battery icon)
      buf[75] = '\0';
    else
      buf[83] = '\0';
    Line_1 = buf + 43;
    NEWMODE = true;
  }
}
