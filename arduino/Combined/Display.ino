void showDateInfo() {
  // sprintf_P(DateString, PSTR("%02d-%02d-1990"), Day, Month); // THIS DOES NOT WORK SOMETIMES
  if (Day < 10)
    lcd.print("0");
  lcd.print(Day);
  lcd.print("-");
  if (Month < 10)
    lcd.print("0");
  lcd.print(Month);
  lcd.print("-1990");
}

void showBatteryIcon(uint8_t percent) {
  uint8_t segments = map(percent, 0, 100, 0, 8);
  if (segments <= 0)
    lcd.write(LEFTEMPTY);
  else
    lcd.write(LEFTFULL);
  for (uint8_t i = 2; i < 8 ; i++) {
    if (segments >= i)
      lcd.write(FULL);
    else
      lcd.write(EMPTY);
  }
  if (segments >= 8)
    lcd.write(RIGHTFULL);
  else
    lcd.write(RIGHTEMPTY);
}

void clearRow(int row) {
  lcd.setCursor(0, row);
  for (uint8_t n = 0; n < 40; n++) {
    lcd.print(" ");
  }
}

void initLCD() {
  lcd.init();
  lcd.clear();

  lcd.createChar(EMPTY, empty);
  lcd.createChar(FULL, full);
  lcd.createChar(LEFTFULL, batteryLFull);
  lcd.createChar(LEFTEMPTY, batteryLEmpty);
  lcd.createChar(RIGHTFULL, batteryRFull);
  lcd.createChar(RIGHTEMPTY, batteryREmpty);
}
