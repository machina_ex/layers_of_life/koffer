#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#define CLK 2
#define DT 3
#define SW 4
#define ENCODERMIN 1
#define ENCODERMAX 365
boolean A_set = false; // interrupt service routine variable
boolean B_set = false; // interrupt service routine variable
volatile unsigned int encoderPos = ENCODERMIN; // a counter for the dial
unsigned int lastReportedPos = ENCODERMIN; // change management
static boolean rotating = false; // debounce management
uint8_t button = LOW;
uint8_t old_button = LOW;

#define OFF  0
#define RESET 1
#define INFO  2
#define SELECT 3
#define TEXT  4
uint8_t MODE = SELECT;
boolean NEWMODE = true;
boolean RLOCKED = true;

int month_yearday[13] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 };
uint8_t Month = 1;
uint8_t Day = 1;
char DateString[10];

uint8_t Battery = 100;
uint8_t lastReportedBattery = 0;

char* Line_0;
char* Line_1;
String Line_1_Label = " Drehen und Druecken zur Auswahl";

LiquidCrystal_I2C lcd(0x27, 40, 2);

void setup() {
  pinMode(CLK, INPUT);
  pinMode(DT, INPUT);
  pinMode(SW, INPUT_PULLUP );
  digitalWrite(CLK, HIGH); // turn on pullup resistors
  digitalWrite(DT, HIGH); // turn on pullup resistors
  attachInterrupt(digitalPinToInterrupt(CLK), doEncoderA, CHANGE);
  attachInterrupt(digitalPinToInterrupt(DT), doEncoderB, CHANGE);

  initLCD();

  Serial.setTimeout(500);
  Serial.begin(115200);
}

void loop() {
  readSerial();

  if (NEWMODE) {
    NEWMODE = false;
    onModeChange();
  }

  switch (MODE) {
    case OFF:
      if (buttonPressed())
        Serial.println(1);
      break;
    case RESET:
      if (buttonPressed())
        Serial.println(1);
      break;
    case INFO:
      lcd.setCursor(0, 0);
      lcd.print(Line_0);
      lcd.setCursor(0, 1);
      showBatteryIcon(Battery);
      lcd.print(Line_1);
      if (buttonPressed())
        Serial.println(1);
      break;
    case SELECT:
      rotating = true; // reset the debouncer
      if (lastReportedPos != encoderPos) {
        lastReportedPos = encoderPos;
        dayToDate();
        lcd.setCursor(15, 0);
        showDateInfo();
      }
      if (lastReportedBattery != Battery) {
        lastReportedBattery = Battery;
        lcd.setCursor(0, 1);
        showBatteryIcon(Battery);
        lcd.print(Line_1_Label);
      }
      lcd.setCursor(26, 0); // set cursor for blink
      if (buttonPressed())
        Serial.println(lastReportedPos);
      break;
    case TEXT:
      lcd.setCursor(0, 0);
      lcd.print(Line_0);
      lcd.setCursor(0, 1);
      lcd.print(Line_1);
      if (buttonPressed())
        Serial.println(1);
      break;
  }
}

void onModeChange() {
  switch (MODE) {
    case OFF:
    case RESET:
      lcd.clear();
      lcd.noBacklight();
      lcd.noBlink();
      Month = 1;
      Day = 1;
      rotating = false;
      button = LOW;
      old_button = LOW;
      RLOCKED = true;
      encoderPos = ENCODERMIN;
      lastReportedPos = ENCODERMIN;
      break;
    case INFO:
      lcd.backlight();
      lcd.clear();
      lcd.noBlink();
      RLOCKED = true;
      break;
    case SELECT:
      lcd.backlight();
      lcd.clear();
      RLOCKED = false;
      dayToDate();
      lcd.setCursor(15, 0);
      showDateInfo();
      lcd.setCursor(0, 1);
      showBatteryIcon(Battery);
      lcd.print(Line_1_Label);
      lcd.blink();
      break;
    case TEXT:
      lcd.backlight();
      lcd.clear();
      break;
  }
}
