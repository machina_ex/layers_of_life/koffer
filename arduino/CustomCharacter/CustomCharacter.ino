#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 40, 2);


void setup() {
  lcd.init();
  lcd.backlight();

  // create a new character
  lcd.createChar(EMPTY, empty);
  lcd.createChar(FULL, full);
  lcd.createChar(LEFTFULL, batteryLFull);
  lcd.createChar(LEFTEMPTY, batteryLEmpty);
  lcd.createChar(RIGHTFULL, batteryRFull);
  lcd.createChar(RIGHTEMPTY, batteryREmpty);

  Serial.begin(9600);

  lcd.setCursor(0, 1);
  showBatteryIcon(100);
  lcd.print("DREHEN & DRUECKEN ZUR AUSWAHL");
}

void loop() {
  // Recieve Data
  if (Serial.available() > 0) {
    String data = Serial.readStringUntil('\n');
    // TODO Read returned dayNumber
    Serial.print("You sent me: ");
    Serial.println(data);

    clearRow(1);
    lcd.setCursor(0, 1);
    showBatteryIcon(data.toInt());
    lcd.print("DREHEN & DRUECKEN ZUR AUSWAHL");
  }
}

void showBatteryInfo(String info) {
  lcd.print(info);
}

void showBatteryIcon(uint8_t percent) {
  uint8_t segments = map(percent, 0, 100, 0, 8);
  lcd.print(" ");
  if (segments <= 0)
    lcd.write(LEFTEMPTY);
  else
    lcd.write(LEFTFULL);
  for (uint8_t i = 2; i < 8 ; i++) {
    if (segments >= i)
      lcd.write(FULL);
    else
      lcd.write(EMPTY);
  }
  if (segments >= 8)
    lcd.write(RIGHTFULL);
  else
    lcd.write(RIGHTEMPTY);
  lcd.print(" ");
}

void clearRow(int row) {
  lcd.setCursor(0, row);
  for (uint8_t n = 0; n < 40; n++) {
    lcd.print(" ");
  }
}
