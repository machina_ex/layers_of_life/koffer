#!/usr/bin/env python3

import socket

import adafruit_thermal_printer
import serial

import paho.mqtt.client as mqtt
import json


def remove_umlaut(string):
    """
    Removes umlauts from strings and replaces them with the letter+e convention
    :param string: string to remove umlauts from
    :return: unumlauted string
    """
    u = 'ü'.encode()
    U = 'Ü'.encode()
    a = 'ä'.encode()
    A = 'Ä'.encode()
    o = 'ö'.encode()
    O = 'Ö'.encode()
    ss = 'ß'.encode()

    string = string.encode()
    string = string.replace(u, b'ue')
    string = string.replace(U, b'Ue')
    string = string.replace(a, b'ae')
    string = string.replace(A, b'Ae')
    string = string.replace(o, b'oe')
    string = string.replace(O, b'Oe')
    string = string.replace(ss, b'ss')

    string = string.decode('utf-8')
    return string

class BonPrinter:
    def __init__(self):
        s = serial.Serial("/dev/ttyUSB0",9600)
        ThermalPrinter = adafruit_thermal_printer.get_printer_class(2.69)
        self.p = ThermalPrinter(s)
        self.p.warm_up()
        self.p.set_defaults()
        self.p.up_down_mode = False

    def print_barcode(self, text):
        ## len needs to be 7 letters
        self.p._set_barcode_height(90)
        self.p.print_barcode(text,self.p.CODE128)
        
    def print_text(self, text):
        self.p.justify = adafruit_thermal_printer.thermal_printer.JUSTIFY_CENTER
        self.p.print(text)

def _on_message(client, userdata , msg):
    print(msg.topic + "=" + str(msg.payload,"utf8"))
    try:
        if msg.topic.startswith(socket.gethostname()+"/print"):
            data = json.loads(str(msg.payload,"utf8"))

            bon_printer.print_text(remove_umlaut(data["TEXT"]))
            bon_printer.print_barcode(str(data["BARCODE"]))
            bon_printer.p.feed(6)

    except Exception as e:
        print(e)


bon_printer = BonPrinter()

mqtt_client = mqtt.Client()
mqtt_client.on_message = _on_message
mqtt_client.connect("localhost",1883)
mqtt_client.subscribe((socket.gethostname()+"/print",2))
mqtt_client.loop_forever()
