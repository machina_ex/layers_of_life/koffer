#!/bin/bash
sudo ln -s /home/pi/player-setup/*.service /etc/systemd/system/
sudo systemctl enable *.service
git submodule update --init --recursive

sudo apt install -y python3-smbus python3-pip
sudo pip3 install lark pyserial-asyncio asyncio-mqtt adafruit-thermal_printer
sudo pip3 install pi-ina219

