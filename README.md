# Koffer

Informationen, Config Files und Software für die Koffer im Theater-Game Layers of Life

## Raspberry Pi Setup

The following steps were performed on a Raspberry Pi 3 B + with Raspberry Pi OS Lite.

### General

- Setup Pi `sudo raspi-config` enable SSH and set Localisation Options
- Update `sudo apt update`
- Install Screen `sudo apt install screen`


### Access Point

- Follow the tutorial on [
Setting up a Routed Wireless Access Point](https://www.raspberrypi.com/documentation/computers/configuration.html#setting-up-a-routed-wireless-access-point) and [Raspberry and USB Tethering](https://peppe8o.com/raspberry-pi-portable-hotspot-with-android-usb-tethering/)
  - _/etc/hostapd/hostapd.conf_ variables set to `ssid=meX-Koffer`
  - _/etc/sysctl.conf_ uncommetn `net.ipv4.ip_forward=1`
  - setup routing to usb `sudo iptables -t nat -A POSTROUTING -o usb0 -j MASQUERADE`
    - save with 
    ```
    sudo su -
    iptables-save > /etc/iptables.rules
    exit
    ```
    - _/etc/rc.local_ add `iptables-restore < /etc/iptables.rules`  

  - _/etc/dnsmasq.conf_ set to
  ```
  interface=wlan0 
    dhcp-range=192.168.4.2,192.168.4.100,255.255.255.0,24h
    domain=wlan
    address=/gw.wlan/192.168.4.1
  ```

  - _/etc/dhcpcd.conf_ set to `static ip_address=192.168.4.1/24`

  - `sudo systemctl unmask hostapd && sudo systemctl enable hostapd && sudo reboot`

#### Setting up Android Phone

- go to "Settings", then tap "About device" or "About phone".
- scroll down, then tap "Build number" seven times to enable _Developer Mode_
- in "Developer Options" set default USB setting to _USB tethering_
- disable phone lock to keep tethering on all the time


### MQTT Broker

- `sudo apt install mosquitto mosquitto-clients`
- To enable websocket on Mosquitto add a _.conf_ file e.g. `sudo nano /etc/mosquitto/conf.d/websocket.conf` with 
```
listener 1883
protocol mqtt

listener 9001
protocol websockets

allow_anonymous true

connection layers-of-life
address 164.92.244.191:1883

topic koffer1/# both 2
topic koffer2/# both 2
topic koffer3/# both 2
topic koffer4/# both 2
topic set/# both 2

remote_username machina
remote_password ...
```
