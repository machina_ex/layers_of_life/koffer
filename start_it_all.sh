#!/bin/bash

sudo modprobe i2c-dev

cd "$(dirname "$0")"

screen -dmS clock
screen -S clock -X stuff "python3 koffer-statemachine/mqttClock.py ^M"

screen -dmS statemachine
screen -S statemachine -X stuff "cd koffer-statemachine/ && python3 mqttStatemachine.py ^M"

screen -dmS arduino
screen -S arduino -X stuff "python3 arduino_mqtt.py ^M"

screen -dmS printer
screen -S printer -X stuff "python3 print_server.py ^M"

screen -dmS battery
screen -S battery -X stuff "python3 read_power.py ^M"
