#!/usr/bin/env python3

import socket
import struct
import json

import asyncio
import serial_asyncio
import asyncio_mqtt as mqtt



def remove_umlaut(string):
    """
    Removes umlauts from strings and replaces them with the letter+e convention
    :param string: string to remove umlauts from
    :return: unumlauted string
    """
    u = 'ü'.encode()
    U = 'Ü'.encode()
    a = 'ä'.encode()
    A = 'Ä'.encode()
    o = 'ö'.encode()
    O = 'Ö'.encode()
    ss = 'ß'.encode()

    string = string.encode()
    string = string.replace(u, b'ue')
    string = string.replace(U, b'Ue')
    string = string.replace(a, b'ae')
    string = string.replace(A, b'Ae')
    string = string.replace(o, b'oe')
    string = string.replace(O, b'Oe')
    string = string.replace(ss, b'ss')

    string = string.decode('utf-8')
    return string


class KofferConnector:


    MODE2NUM = {"OFF":0,"RESET":1,"INFO":2,"SELECT":3,"TEXT":2}
    
    def __init__(self, mqtt):
        self.mqtt = mqtt
        self.koffer = socket.gethostname()
        self.time_counter = 0
        self.starttime = 0
        pass

    async def connect(self, url = '/dev/ttyUSB0'):
        self.reader , self.writer = await serial_asyncio.open_serial_connection(url=url, baudrate=115200)
        asyncio.ensure_future(self._reader_task())


    async def _reader_task(self):
        while True:
            msg = await self.reader.readline()
            print(msg)
            if msg.decode('utf-8')[:-2].isnumeric():
                await self.mqtt.publish(self.koffer+"/day", msg.decode('utf-8'))
                await self.mqtt.publish(self.koffer+"/lock", "LOCK")


    async def set_state(self, state):
    
        mode = self.MODE2NUM[state["MODE"]]
        battery = int(100-100*((self.time_counter-self.starttime)/(42*60)))

        s = b""
        if "LINE_1" in state:
            s += bytes(remove_umlaut(state["LINE_1"])[0:39],'utf8')+b"\0"
        if "LINE_2" in state:
            s += bytes(remove_umlaut(state["LINE_2"])[40:79],"utf8")+b"\0"
        
        self.writer.write(struct.pack("BB", mode, battery)+s)
        await asyncio.sleep(0.5)
        
    async def listen_to_state_messages(self):
        async with self.mqtt.filtered_messages(self.koffer+"/arduino") as messages:
            await self.mqtt.subscribe(self.koffer+"/arduino")
            async for message in messages:
                var = json.loads(message.payload.decode("utf-8"))
                await self.set_state(var)
                
    async def listen_to_time_message(self):
        async with self.mqtt.filtered_messages(self.koffer+"time/counter") as messages:
            await self.mqtt.subscribe(self.koffer+"time/counter")
            async for message in messages:
                self.time_counter = int(message.payload.decode("utf-8"))
                
    async def listen_to_starttime_message(self):
        async with self.mqtt.filtered_messages(self.koffer+"/starttime") as messages:
            await self.mqtt.subscribe(self.koffer+"/starttime")
            async for message in messages:
                self.starttime = int(message.payload.decode("utf-8"))  


if __name__ == '__main__':
    async def main():
        async with mqtt.Client("localhost") as mqtt_client:
            k = KofferConnector(mqtt_client)
            await k.connect("/dev/ttyACM0")
            asyncio.ensure_future(k.listen_to_time_message())
            asyncio.ensure_future(k.listen_to_starttime_message())
            asyncio.ensure_future(k.listen_to_state_messages())

            #block task
            while True:
                await asyncio.sleep(10)

    loop = asyncio.get_event_loop()      
    loop.run_until_complete(main())
    loop.close()
